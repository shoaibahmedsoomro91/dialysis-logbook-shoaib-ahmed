import React from "react";
import { HStack, Text, InfoIcon } from 'native-base';

interface EntryDetailProps {
  item : any
}

export default function EntryDetail({ item } : EntryDetailProps ) {  
  
  return (
    <>
      {
        item?.minValue && item?.minValue && 
        <HStack space={5}>
          <Text _dark={{
            color: "warmGray.50"
          }} color="coolGray.800" bold>
            {`Min Value : ${item?.minValue}`}
          </Text>                
          <Text _dark={{
              color: "warmGray.50"
            }} color="coolGray.800" bold>                    
            {`Max Value : ${item?.maxValue}`}
          </Text>
        </HStack>                
      } 
      {
        item?.metadataFields[1] &&
        <HStack alignItems="center" space={1}>
          <InfoIcon _light={{ color : "emerald.800" }} _dark={{ color : "emerald.300" }} />
          <Text _dark={{
            color: "warmGray.50"
          }} color="coolGray.800" bold>
            {item?.metadataFields[1]}
          </Text>
        </HStack>
                                      
      }
      {
        item?.metadataFields[0] && 
        <HStack>              
          <Text fontSize="xs" _dark={{
              color: "warmGray.50"
            }} color="coolGray.800">
            {item?.metadataFields[0]}
          </Text>                
        </HStack>
      
      }
    </>
  );
}
