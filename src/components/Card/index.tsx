import React from 'react';
import {
  Heading,
  Pressable,  
  Divider
} from 'native-base';
import EntryDetail from '../EntryDetail';

interface CardProps {
  item : any,
  handleClick : (() => void)
}

export default function Card({ item, handleClick }: CardProps) {
  return (  
    <Pressable
      w="100%" 
      bg="coolGray.200"
      rounded="md" 
      shadow={1}  
      pr={["0", "5"]} 
      py="5" 
      px="2"
      flex={1}
      onPress={() => {
        handleClick();
      }}      
    >    
      <Heading size="xs">
        {item.heading}
      </Heading>
      {
        item.latest_entry.value &&
        <>
          <Divider my="2" _light={{ bg: "muted.800"}} _dark={{ bg: "muted.50" }} />
          <EntryDetail item={item.latest_entry.value}></EntryDetail> 
        </>
      }
        
    </Pressable>    
  );
}
