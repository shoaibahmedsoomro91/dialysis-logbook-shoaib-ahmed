import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Box } from 'native-base';
import { RootStack } from '../../src/navigators/rootNavigator';

export const Root = () => {
  

  return (
    <NavigationContainer>
      <Box
        flex={1}
        w="100%"
        _light={{
          bg: 'coolGray.50',
        }}
        _dark={{
          bg: 'blueGray.900',
        }}
        _web={{
          overflowX: 'hidden',
        }}
      >
        <RootStack />
      </Box>
    </NavigationContainer>
  );
};
