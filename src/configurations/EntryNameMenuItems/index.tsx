export const EntryNameMenuItems = [
  {
    label: "POTASSIUM",
    value: "POTASSIUM"
  },
  {
    label: "PHOSPHATE",
    value: "PHOSPHATE"
  },
  {
    label: "WEIGHT",
    value: "WEIGHT"
  },
  {
    label: "WELLBEING",
    value: "WELLBEING"
  },
  {
    label: "ITCH_INTENSITY",
    value: "ITCH_INTENSITY"
  }
]