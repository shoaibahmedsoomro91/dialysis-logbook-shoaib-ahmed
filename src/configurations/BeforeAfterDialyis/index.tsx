export const BeforeAfterDialysis = [
  {
    label: "Arbitrary",
    value: "Arbitrary"
  },
  {
    label: "After Dialysis",
    value: "After Dialysis"
  },
  {
    label: "Before Dialysis",
    value: "Before Dialysis"
  }
]