export const DialysisTypes = [
  {
    type: "NUMERIC",
    name: "POTASSIUM",
    heading : "POTASSIUM",
    latest_entry: {}
  },
  {
    type: "NUMERIC",
    name: "PHOSPHATE",
    heading : "PHOSPHATE",
    latest_entry: {}
  },
  {
    type: "NUMERIC",
    name: "WEIGHT",
    heading : "WEIGHT",
    latest_entry: {}
  },
  {
    type: "INTEGER",
    name: "WELLBEING",
    heading : "WELLBEING",
    latest_entry: {}
  },
  {
    type: "INTEGER",
    name: "ITCH_INTENSITY",
    heading : "ITCH INTENSITY",
    latest_entry: {}
  }
]
