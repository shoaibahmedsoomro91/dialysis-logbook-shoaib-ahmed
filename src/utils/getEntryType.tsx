import { DialysisTypes } from '../configurations';

export function getEntryType(name : string | null) {	
  const entry = DialysisTypes.find(item => item.name === name)
  if(entry)
  return entry['type']
  
  return null;
}