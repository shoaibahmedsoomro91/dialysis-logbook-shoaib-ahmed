import React from "react";
import { Layout, Loading, Dialog } from "../../components";
import { TextField, TextAreaField, SelectField } from "../../components/Inputs";
import { Stack, FormControl } from 'native-base';
import { useForm, Controller } from 'react-hook-form';
import { Button } from 'react-native';
import { useDispatch } from 'react-redux'
import { yupResolver } from '@hookform/resolvers/yup';
import { addValue } from '../../../logbook-state'
import { createEntrySchema } from '../../utils/createSchema';
import { getEntryType } from '../../utils/getEntryType';
import { BeforeAfterDialysis, EntryNameMenuItems } from '../../configurations';
type FormData = {   
  minValue?: string,
  maxValue?: string,
  dialysis_type : string,
  notes : string
};

export default function AddEntry({ route, navigation } : any) {
  const dispatch = useDispatch();
  const [ entryName, setEntryName ] = React.useState<string | null>(null);
  const [ entryType, setEntryType ] = React.useState<string | null>(null);
  const [ isSubmit, setIsSubmit ] = React.useState(false);
  const [ loading, setLoading ] = React.useState(false);
  const [ showDialog, setShowDialog ] = React.useState(false);
  const schema = createEntrySchema(entryType);
  const showDropDown = route.params.showEntryNameDropdowm;

  const { handleSubmit, control , formState: { errors } } = useForm<FormData>({
    resolver: yupResolver(schema)
  });

  const onSubmit = (data: FormData) => {
    setIsSubmit(true);
    setLoading(true);
    const { dialysis_type, maxValue, minValue, notes } = data;    
    const form_data  = {
     id : Math.random(),
     value : {
      type : entryType,
      name : entryName,
      metadataFields : [ notes, dialysis_type ],
      minValue : minValue,
      maxValue : maxValue
     }
    }    
    dispatch(addValue(form_data));   
    setLoading(false);
    setShowDialog(true);
  };
  
  const handleDialogButton = () => {
    navigation.navigate('Home')
  }

  const handleEntryNameChange = (value : string) => {    
    const type = getEntryType(value);
    setEntryType(type);
    setEntryName(value)
  }

  React.useEffect(() => {
    setEntryName(route.params.name);
    setEntryType(route.params.type)
  },[])

  return (
    <Layout>
      {
        showDialog && <Dialog handleButtonClick={handleDialogButton}></Dialog>
      }      
      { 
        loading && <Loading></Loading> 
      }
      <Stack space={5}>
        {
          showDropDown &&
          <Stack>
            <FormControl.Label>Entry Name</FormControl.Label>
            <SelectField value={entryName || ''} placeholder="Choose Entry Name" items={EntryNameMenuItems} onChange={( value : string )=>{handleEntryNameChange(value)}} error = {null}></SelectField> 
          </Stack>
        }        
        <Stack>
          <FormControl.Label>Min Value</FormControl.Label>
          <Controller
            control={control}
            render={({field: { onChange, value }}) => (
              <TextField 
                value = {value || ''} 
                variant = "underlined"
                placeholder = "Min Value"
                onChange={( value : string )=>{onChange(value)}}
                error = {errors?.minValue}
                keyboardType= 'numeric'
              ></TextField>                          
            )}
            name="minValue"
            rules={{ required: true }}
          />            
        </Stack> 
        <Stack>
          <FormControl.Label>Max Value</FormControl.Label>
          <Controller
            control={control}
            render={({field: { onChange, value }}) => (
              <TextField
                value = {value || ''} 
                variant = "underlined"
                placeholder = "Max Value"
                onChange={( value : string )=>{onChange(value)}}
                error = {errors?.maxValue}
                keyboardType= 'numeric'
              ></TextField>
            )}
            name="maxValue"
            rules={{ required: true }}
          />
        </Stack>
        <Stack>
          <FormControl.Label>Dialysis type</FormControl.Label>
          <Controller
            control={control}
            render={({field: { onChange, value }}) => (
              <SelectField value={value} placeholder="Choose dialysis type" items={BeforeAfterDialysis} onChange={( value : string )=>{onChange(value)}} error = {errors?.dialysis_type}></SelectField>)}
            name="dialysis_type"
            rules={{ required: true }}
          /> 
        </Stack>
        <Stack>
          <FormControl.Label>Notes</FormControl.Label>            
          <Controller
            control={control}
            render={({field: { onChange, onBlur, value }}) => (
              <TextAreaField value = {value} placeholder = "Enter Notes of you dialysis" onChange={( value : string )=>{onChange(value)}}></TextAreaField>
            )}
            name="notes"
            rules={{ required: true }}
          />
        </Stack>
        <Stack>          
          <Button disabled = {isSubmit} title="Submit" onPress={handleSubmit(onSubmit)}/>
        </Stack>
      </Stack>
    </Layout>
  );
}