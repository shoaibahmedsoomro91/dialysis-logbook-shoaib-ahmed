import React from "react";
import type { RootState } from '../../../store';
import { useSelector } from 'react-redux'
import { Layout, Loading, AppButton, NotificationLabel, EntryDetail } from "../../components";
import { VStack, FlatList, Box } from 'native-base';
import ILogBook from "../../types/LogBook";

interface EntriesProps {
  route : any,
  navigation: any
}

export default function Entries({ route, navigation } : EntriesProps ) {  
  const logbook_values = useSelector((state: RootState) => state.logbook.value)
  const [ values, setValues ] = React.useState<Array<ILogBook>>([]);
  const [ loading, setLoading ] = React.useState(true);
  const name : string = route.params.name;
  const type : string = route.params.type;
  const heading : string = route.params.heading;
  
  const handleBtnClick = () => {
    navigation.navigate('AddEntry',{
      name : name,
      type : type,
      showEntryNameDropdowm : false
    })
  }
  React.useEffect(() => {
    const filter_by_name = logbook_values.filter((item) => {
      return item.value.name === name
    });
    setValues(filter_by_name);
    setLoading(false)    
  },[])

  React.useEffect(() => {
    navigation.setOptions({
      title: heading === '' ? 'No title' : heading,
    });
  }, [navigation, heading]);

  return (
    <Layout>
      <AppButton label ='Add new entry' handleClick={handleBtnClick}></AppButton>      
      { 
        loading && <Loading></Loading> 
      }      
      {
        !loading && values.length > 0 && <VStack flex={1}>
          <FlatList data={values} renderItem={({item}) => 
            {
              return (
                <Box borderBottomWidth="1" pl={["0", "4"]} pr={["0", "5"]} py="2">
                  <EntryDetail item={item.value}></EntryDetail>                  
                </Box>
              )
            }     
          }>
          </FlatList>
        </VStack>
      }
      {
        !loading && values.length === 0 && <NotificationLabel/>
      }        
    </Layout>
  );
}
