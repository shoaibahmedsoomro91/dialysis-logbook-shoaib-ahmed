import React from "react";
import type { RootState } from '../../../store';
import { useSelector } from 'react-redux';
import { StackNavigationProp } from "@react-navigation/stack";
import { Layout, Card, AppButton } from "../../components";
import { HStack, VStack, ScrollView } from 'native-base';
import ILogBookTypes from '../../types/LogBookTypes'

export default function Home({
  navigation,
}: {
  navigation: StackNavigationProp<any>;
}) {
  const logbook_types = useSelector((state: RootState) => state.logbook.types);
  const [ type, setType ] = React.useState<Array<ILogBookTypes>>([]);
  const handleClick = (dt : any) => {
    navigation.navigate('Entries', {
      name: dt.name,
      type : dt.type,
      heading : dt.heading
    })
  }
  const handleBtnClick = () => {
    navigation.navigate('AddEntry',{
      name : null,
      type : null,
      showEntryNameDropdowm : true
    })
  }

  React.useEffect( () => {
    const unsubscribe = navigation.addListener("focus",()=>{      
      setType(logbook_types);
    });
    return unsubscribe;
  },[navigation,logbook_types])
  return (
    <Layout>      
      <VStack space={3} flex={1}>
        <AppButton label ='Add new entry' handleClick={handleBtnClick}></AppButton>
        <ScrollView>
          <VStack space={3}>
            {
              type.map((dt,index) => {
                return <HStack key={index}>
                  <Card item={dt} handleClick={ ()=> { handleClick(dt)}}></Card>
                </HStack>    
              })
            }
          </VStack>          
        </ScrollView>                
      </VStack>
    </Layout>
  );
}
