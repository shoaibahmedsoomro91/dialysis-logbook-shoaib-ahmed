import { createSlice, PayloadAction, createAsyncThunk } from "@reduxjs/toolkit";
import ILogBook from "./src/types/LogBook";
import ILogBookTypes from "./src/types/LogBookTypes";
import { DialysisTypes } from './src/configurations'
/**
 * TODO:
 * Manage redux reducers here and add them to the logbookSlice
 * The reducer is already added to the store.ts file
 */

 export interface LogBookState {
  types: Array<ILogBookTypes>
  value: Array<ILogBook>
}

const initialState: LogBookState = {
  types : DialysisTypes,
  value: []
}



const logbookSlice = createSlice({
  name: "logbook",
  initialState: initialState,
  reducers: {    
    addValue : (state, action) => {            
      const index = state.types.findIndex(x => x.name === action.payload.value.name);      
      if(index > -1){
        state.types[index].latest_entry = action.payload;
      } 
      state.value.push(action.payload);
    }
  },
});

export const { addValue } = logbookSlice.actions
export const logbookReducer = logbookSlice.reducer;
